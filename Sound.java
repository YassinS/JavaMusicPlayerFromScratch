import javax.sound.sampled.*;
import javax.swing.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;



public class Sound implements LineListener {

    boolean playCompleted;
    boolean debug = false;
    public Clip clip;
    public void playSound(String path) {
        File f = new File(path);
        try {
            AudioInputStream audioStream = AudioSystem.getAudioInputStream(f);
            AudioFormat format = audioStream.getFormat();
            DataLine.Info info = new DataLine.Info(Clip.class, format);
            clip = (Clip) AudioSystem.getLine(info);
            clip.addLineListener((LineListener) this);
            clip.open(audioStream);
            clip.start();
            while (!playCompleted) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ie) {
                    ie.printStackTrace();
                }

            }
            clip.close();

        } catch (UnsupportedAudioFileException e) {
            System.out.println("The format of your audio file has to be .wav");
            JOptionPane.showMessageDialog(null, "The format of your audio file has to be .wav  \n   "
                    + e);
            if (debug) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException fnfe) {
            JOptionPane.showMessageDialog(null, "The file doesn't exist \n " + fnfe);
            if (debug) {
                fnfe.printStackTrace();

            }
        } catch (LineUnavailableException e) {
            JOptionPane.showMessageDialog(null, "Audio line is not accessible. " +
                    "Close other audio programs and try again.\n" + e);
            if (debug) {
                e.printStackTrace();
            }
        } catch (IOException ioe) {
            System.out.println("An error occurred while processing the audio file");
            JOptionPane.showMessageDialog(null, "An error occurred while processing the audio file\n" + ioe);
            if (debug) {
                ioe.printStackTrace();
            }
        }

    }

    public void play(){
        clip.setFramePosition(0);
        clip.start();

    }
    public void stop(){
       clip.stop();

    }

    static int i;

    public void loop(){
        i++;
       clip.loop(Clip.LOOP_CONTINUOUSLY);
       if(i%2 == 0){
           clip.loop(0);
       }
    }


    @Override
    public void update(LineEvent event) {
        LineEvent.Type type = event.getType();

        if (type == LineEvent.Type.START) {
            System.out.println("Playback started");
        }

        if (type == LineEvent.Type.STOP) {
            System.out.println("Playback stopped");
        }
    }
}
